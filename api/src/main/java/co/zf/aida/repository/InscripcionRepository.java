package co.zf.aida.repository;

import co.zf.aida.model.Inscripcion;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    @Query(value = "SELECT m FROM Inscripcion m WHERE m.estado = 3")
    List <Inscripcion> findByEstado();
}
