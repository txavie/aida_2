package co.zf.aida.controller;

import co.zf.aida.exception.ResourceNotFoundException;
import co.zf.aida.model.Inscripcion;
import co.zf.aida.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600,allowedHeaders="*",allowCredentials="")
@RequestMapping("/api")
public class AidaController {

    @Autowired
    InscripcionRepository noteRepository;

    @GetMapping("/notes")
    public List<Inscripcion> getAllNotes() {
        return noteRepository.findByEstado();
    }

    @PostMapping(path = "/notes", consumes = "application/json", produces = "application/json")
    public Inscripcion createNote(@Valid @RequestBody Inscripcion note) {
        return noteRepository.save(note);
    }

    @GetMapping("/notes/{id}")
    public Inscripcion getNoteById(@PathVariable(value = "id") Long noteId) {
        return noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
    }

    @PutMapping("/notes")
    public Inscripcion updateNote(@RequestBody Inscripcion noteDetails) {
        Inscripcion note = noteRepository.findById(noteDetails.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteDetails.getId()));

        note.setEstado(noteDetails.getEstado());
//        note.setContent(noteDetails.getContent());
        Inscripcion updatedNote = noteRepository.save(note);
        return updatedNote;
    }

    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
        Inscripcion note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

        noteRepository.delete(note);

        return ResponseEntity.ok().build();
    }
}
